package com.rebtel.countryinfo.model;

/**
 * Created by rahul on 08/05/16.
 * Model class which holds country details
 */
public class Country {
    private String mName;
    private String mRegion;
    private String mCapital;
    private String mCountryCode;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getRegion() {
        return mRegion;
    }

    public void setRegion(String region) {
        this.mRegion = region;
    }

    public String getCapital() {
        return mCapital;
    }

    public void setCapital(String capital) {
        this.mCapital = capital;
    }

    public String getCountryCode() {
        return mCountryCode;
    }

    public void setCountryCode(String mCountryCode) {
        this.mCountryCode = mCountryCode;
    }
}
