package com.rebtel.countryinfo.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ProgressBar;

import com.rebtel.countryinfo.R;
import com.rebtel.countryinfo.utils.GenericUtils;

/**
 * Created by rahul on 08/05/16.
 * The AutoCompleteTextVIew, which will list the country names and region based on user query
 */
public class CountrySuggestionTextView extends AutoCompleteTextView {

    private static final int KEY_CODE = 100;
    private ProgressBar mProgressbar;

    public CountrySuggestionTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setLoadingIndicator(ProgressBar progressBar) {
        mProgressbar = progressBar;
    }

    @Override
    protected void performFiltering(CharSequence text, int keyCode) {
        if (!GenericUtils.isNetworkConnected(getContext())) {
            GenericUtils.showShortToast(getContext(), R.string.toast_no_connectivity);
        }
        if (mProgressbar != null) {
            mProgressbar.setVisibility(View.VISIBLE);
        }

        super.performFiltering(text, KEY_CODE);
    }

    @Override
    public void onFilterComplete(int count) {
        if (mProgressbar != null) {
            mProgressbar.setVisibility(View.GONE);
        }
        if (count == 0) {
            GenericUtils.showShortToast(getContext(), R.string.toast_no_country);
        }

        super.onFilterComplete(count);
    }
}
