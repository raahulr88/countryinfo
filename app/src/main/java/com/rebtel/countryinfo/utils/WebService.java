package com.rebtel.countryinfo.utils;

import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by rahul on 08/05/16.
 * Helper class for webservice connectivity
 */
class WebService {

    /**
     * Connect to webservice
     *
     * @param apiEndpoint The api end point
     * @param param       Additional request params
     * @return Response json as String
     */
    @SuppressWarnings("SameParameterValue")
    public static String connect(String apiEndpoint, String param) {
        URL url;
        String response = "";
        try {
            url = new URL(apiEndpoint + param);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }
        } catch (Exception e) {
            Log.e(CountryInfoConstants.LOG_TAG, "Failed to connect to webservice" + e.getMessage());
        }
        return response;
    }
}
