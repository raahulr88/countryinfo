package com.rebtel.countryinfo.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import android.util.Log;

import com.rebtel.countryinfo.model.Country;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahul on 08/05/16.
 * Api to query for country details
 */
public class GetCountryInfoApi {

    /**
     * @param query The search string
     * @return List of Country objects with details filled in
     */
    @Nullable
    @WorkerThread
    public List<Country> getCountryInfo(@NonNull String query) {
        String response = WebService.connect(CountryInfoConstants.COUNTRY_SUBSTRING_API, query);

        if (TextUtils.isEmpty(response)) {
            Log.e(CountryInfoConstants.LOG_TAG, "No response or empty response from HTTP Request");
            return null;
        }

        List<Country> countries = null;
        try {
            countries = parseResponse(response);
        } catch (JSONException e) {
            Log.e(CountryInfoConstants.LOG_TAG, e.getMessage());
        }

        return countries;
    }


    private List<Country> parseResponse(String jsonString) throws JSONException {
        JSONArray jsonArray = new JSONArray(jsonString);

        if (jsonArray.length() == 0) {
            Log.e(CountryInfoConstants.LOG_TAG, "No response or empty response from HTTP Request");
            return null;
        }
        List<Country> countries = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            Country country = new Country();
            country.setName(jsonObject.getString(CountryInfoConstants.TAG_NAME));
            country.setRegion(jsonObject.getString(CountryInfoConstants.TAG_REGION));
            country.setCapital(jsonObject.getString(CountryInfoConstants.TAG_CAPITAL));
            country.setCountryCode(jsonObject.getString(CountryInfoConstants.TAG_COUNTRY_CODE));
            countries.add(country);
        }
        return countries;
    }
}
