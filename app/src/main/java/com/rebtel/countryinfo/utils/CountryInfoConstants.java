package com.rebtel.countryinfo.utils;

/**
 * Created by rahul on 08/05/16.
 * The constants for the application
 */
public class CountryInfoConstants {
    public static final String GOOGLE_SEARCH_API = "https://www.google.se/?l#q=";
    public static final String COUNTRY_SUBSTRING_API = "https://restcountries.eu/rest/v1/name/";
    public static final String TAG_NAME = "name";
    public static final String TAG_CAPITAL = "capital";
    public static final String TAG_REGION = "region";
    public static final String TAG_COUNTRY_CODE = "alpha2Code";
    public static final String LOG_TAG = "CountryInfo";
}
