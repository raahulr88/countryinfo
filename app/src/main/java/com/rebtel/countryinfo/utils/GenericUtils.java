package com.rebtel.countryinfo.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.widget.Toast;

/**
 * Created by rahul on 08/05/16.
 * Generic
 */
public class GenericUtils {
    /**
     * @param context Context
     * @return true if there is network connectivity, false otherwise
     */
    public static boolean isNetworkConnected(@NonNull Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    /**
     * @param context Context
     * @param resId   The String resource id
     */
    public static void showShortToast(@NonNull Context context, @StringRes int resId) {
        Toast.makeText(context, context.getText(resId), Toast.LENGTH_SHORT).show();

    }
}
