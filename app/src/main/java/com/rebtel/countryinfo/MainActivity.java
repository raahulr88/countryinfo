package com.rebtel.countryinfo;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.rebtel.countryinfo.fragment.CountryDetailsFragment;
import com.rebtel.countryinfo.fragment.SearchFragment;
import com.rebtel.countryinfo.model.Country;
import com.rebtel.countryinfo.utils.CountryInfoConstants;

public class MainActivity extends AppCompatActivity implements SearchFragment.SearchResultListener {

    private CountryDetailsFragment mCountryDetailsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mCountryDetailsFragment = (CountryDetailsFragment) getFragmentManager().findFragmentById(R.id.fragment_country_details);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if (fab == null) {
            return;
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openBrowser(view);
            }
        });
    }


    @Override
    public void onCountrySelected(Country selectedCountry) {
        mCountryDetailsFragment.refresh(selectedCountry);
    }

    private void openBrowser(View view) {
        Country selectedCountry = mCountryDetailsFragment.getSelectedCountry();
        if (selectedCountry == null) {
            Snackbar.make(view, getText(R.string.snackbar_no_country_selected), Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show();
            return;
        }

        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(CountryInfoConstants.GOOGLE_SEARCH_API + mCountryDetailsFragment.getSelectedCountry().getName()));
        startActivity(browserIntent);
    }
}
