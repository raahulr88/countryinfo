package com.rebtel.countryinfo.fragment;

import android.app.Fragment;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rebtel.countryinfo.R;
import com.rebtel.countryinfo.model.Country;

/**
 * Created by rahul on 08/05/16.
 * The fragment which displays the country detail flag
 */
public class CountryDetailsFragment extends Fragment {

    private Country mSelectedCountry;

    private ImageView mFlagImageView;
    private TextView mCountryNameTextView;
    private TextView mCapitalTextView;
    private TextView mRegionTextView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_country_details, container, false);
        mFlagImageView = (ImageView) rootView.findViewById(R.id.country_flag);
        mCountryNameTextView = (TextView) rootView.findViewById(R.id.country_name);
        mCapitalTextView = (TextView) rootView.findViewById(R.id.country_capital);
        mRegionTextView = (TextView) rootView.findViewById(R.id.country_region);
        return rootView;
    }

    public void refresh(Country selectedCountry) {
        mSelectedCountry = selectedCountry;
        mCountryNameTextView.setText(selectedCountry.getName());
        mCapitalTextView.setText(selectedCountry.getCapital());
        mRegionTextView.setText(selectedCountry.getRegion());
        String countryCode = selectedCountry.getCountryCode().toLowerCase();
        Resources resources = getResources();

        // Work around as Android doesn't permit drawables with name of reserved keyword
        if (countryCode.equalsIgnoreCase("do")) {
            countryCode = "dom";
        }
        final int resourceId = resources.getIdentifier(countryCode, "drawable",
                getActivity().getPackageName());
        Drawable flag;
        try {
            flag = ContextCompat.getDrawable(getActivity(), resourceId);
        } catch (Resources.NotFoundException exception) {
            Log.e("tag", "Flag image resource not found");
            flag = ContextCompat.getDrawable(getActivity(), R.drawable.default_flag);
        }
        mFlagImageView.setImageDrawable(flag);
    }

    @Nullable
    public Country getSelectedCountry() {
        return mSelectedCountry;
    }
}
