package com.rebtel.countryinfo.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;

import com.rebtel.countryinfo.R;
import com.rebtel.countryinfo.adapter.CountrySuggestionAdapter;
import com.rebtel.countryinfo.model.Country;
import com.rebtel.countryinfo.view.CountrySuggestionTextView;

/**
 * Created by rahul on 08/05/16.
 * The fragment which displays the search text box
 */
public class SearchFragment extends Fragment {

    private SearchResultListener mSearchResultListener;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        mSearchResultListener = (SearchResultListener) getActivity();
        View rootView = inflater.inflate(R.layout.fragment_search, container, false);
        final CountrySuggestionTextView queryTextView = (CountrySuggestionTextView) rootView.findViewById(R.id.search_query);
        queryTextView.setThreshold(2);
        queryTextView.setAdapter(new CountrySuggestionAdapter(getActivity()));
        queryTextView.setLoadingIndicator(
                (android.widget.ProgressBar) rootView.findViewById(R.id.search_loading_indicator));
        queryTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Country country = (Country) adapterView.getItemAtPosition(position);
                queryTextView.setText(country.getName());
                mSearchResultListener.onCountrySelected(country);
                InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(adapterView.getApplicationWindowToken(), 0);
            }
        });
        return rootView;
    }

    public interface SearchResultListener {
        void onCountrySelected(Country selectedCountry);
    }
}
