package com.rebtel.countryinfo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.rebtel.countryinfo.R;
import com.rebtel.countryinfo.model.Country;
import com.rebtel.countryinfo.utils.GetCountryInfoApi;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahul on 08/05/16.
 * The adapter for CountrySuggestionTextView
 */
public class CountrySuggestionAdapter extends BaseAdapter implements Filterable {

    private final Context mContext;
    private final GetCountryInfoApi mCountryInfoApi;
    private List<Country> mCountries;

    public CountrySuggestionAdapter(Context context) {
        mContext = context;
        mCountries = new ArrayList<>();
        mCountryInfoApi = new GetCountryInfoApi();
    }

    @Override
    public int getCount() {
        return mCountries.size();
    }

    @Override
    public Country getItem(int index) {
        return mCountries.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.view_country_suggestion_drop_down, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.nameTextView = (TextView) convertView.findViewById(R.id.suggestion_name);
            viewHolder.regionTextView = (TextView) convertView.findViewById(R.id.suggestion_region);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        String countryName = getItem(position).getName();
        String region = getItem(position).getRegion();

        viewHolder.nameTextView.setText(countryName);
        viewHolder.regionTextView.setText(region);
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint == null) {
                    return filterResults;
                }
                List<Country> countries = findCountries(constraint.toString());
                filterResults.values = countries;
                filterResults.count = countries.size();
                return filterResults;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    mCountries = (List<Country>) results.values;
                    notifyDataSetChanged();
                    return;
                }
                notifyDataSetInvalidated();
            }
        };
    }

    private List<Country> findCountries(String query) {
        return mCountryInfoApi.getCountryInfo(query);
    }

    static class ViewHolder {
        TextView nameTextView;
        TextView regionTextView;
    }
}
